package com.example.demo.controller;

import com.example.demo.model.City;
import com.example.demo.service.ICityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class MyController {

    @Autowired
    private ICityService cityService;

    @GetMapping("/")
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/cities")
    public ModelAndView showCities(){
        List<City> cities = cityService.findAll();

        Map<String, Object> params = new HashMap<>();
        params.put("cities", cities);

        return new ModelAndView("cities", params);

    }

    @GetMapping("/form")
    public String form(Model model){
        return "form";
    }
}
